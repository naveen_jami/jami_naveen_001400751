/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;
/**
 *
 * @author navee
 */
public class PersonProfile {
    
    private String name;
    private String geographicData;
    private String dob;
    private String phonenumber;
    private String phonenumberoptional;
    private String faxNumber;
    private String emailAddress;
    private String emailAddress1;
    private String socialSecurityNumber;
    private String medicalRecordNumber;
    private String healthPlanBeneficiaryNumber;
    private String bankAccountNumber;
    private String bankAccountNumber1;
    private String linkedIn;
    private String ipAddress;
    private String ipAddress1;
    private String FullFacesPhoto;
    private String FullFacesPhoto1;

    public PersonProfile() { 
        
        
    }
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGeographicData() {
        return geographicData;
    }

    public void setGeographicData(String geographicData) {
        this.geographicData = geographicData;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public String getPhonenumberoptional() {
        return phonenumberoptional;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public void setPhonenumberoptional(String phonenumberoptional) {
        this.phonenumberoptional = phonenumberoptional;
    }

 

    public String getFaxNumber() {
        return faxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        this.faxNumber = faxNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmailAddress1() {
        return emailAddress1;
    }

    public void setEmailAddress1(String emailAddress1) {
        this.emailAddress1 = emailAddress1;
    }


    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public String getHealthPlanBeneficiaryNumber() {
        return healthPlanBeneficiaryNumber;
    }

    public void setHealthPlanBeneficiaryNumber(String healthPlanBeneficiaryNumber) {
        this.healthPlanBeneficiaryNumber = healthPlanBeneficiaryNumber;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankAccountNumber1() {
        return bankAccountNumber1;
    }

    public void setBankAccountNumber1(String bankAccountNumber1) {
        this.bankAccountNumber1 = bankAccountNumber1;
    }



    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress1() {
        return ipAddress1;
    }

    public void setIpAddress1(String ipAddress1) {
        this.ipAddress1 = ipAddress1;
    }

    public String getFullFacesPhoto() {
        return FullFacesPhoto;
    }

    public void setFullFacesPhoto(String FullFacesPhoto) {
        this.FullFacesPhoto = FullFacesPhoto;
    }

    public String getFullFacesPhoto1() {
        return FullFacesPhoto1;
    }

    public void setFullFacesPhoto1(String FullFacesPhoto1) {
        this.FullFacesPhoto1 = FullFacesPhoto1;
    }

 
    
    
    
}
