/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import Business.PersonProfile;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;

/**
 *
 * @author navee
 */
public class ViewPersonJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewPersonJPanel
     */
    
    public ViewPersonJPanel(PersonProfile person) {
        initComponents();
        displayPersonProfile(person);
        
    }
    
    public void displayPersonProfile(PersonProfile person){
        
    String name = person.getName();
    System.out.println(name);
    String geographicData= person.getGeographicData();
    System.out.println(geographicData);
    String dob = person.getDob();
    System.out.println(dob);
    String phonenumber = person.getPhonenumber();
    System.out.println(phonenumber);
    String phonenumber1 = person.getPhonenumberoptional();
    System.out.println(phonenumber1);
    String faxNumber = person.getFaxNumber();
    System.out.println(faxNumber);
    String emailAddresses = person.getEmailAddress();
    System.out.println(emailAddresses);
    String emailAddresses1 = person.getEmailAddress1();
    System.out.println(emailAddresses1);
    String socialSecurityNumber = person.getSocialSecurityNumber();
    System.out.println(socialSecurityNumber);
    String medicalRecordNumber = person.getMedicalRecordNumber();
    System.out.println(medicalRecordNumber);
    String healthPlanBeneficiaryNumber = person.getHealthPlanBeneficiaryNumber();
    System.out.println(healthPlanBeneficiaryNumber);
    String bankAccountNumbers = person.getBankAccountNumber();
    System.out.println(bankAccountNumbers);
    String bankAccountNumbers1 = person.getBankAccountNumber1();
    System.out.println(bankAccountNumbers1);
    String linkedIn = person.getLinkedIn();
    System.out.println(linkedIn);
    String ipAddresses= person.getIpAddress();
    System.out.println(ipAddresses);
    String ipAddresses1= person.getIpAddress1();
    System.out.println(ipAddresses1);
    String FullFacesPhotos = person.getFullFacesPhoto();
    System.out.println(FullFacesPhotos);
    String FullFacesPhotos1 = person.getFullFacesPhoto1();
    System.out.println(FullFacesPhotos1);
    
    txtNameView.setText(name);
    txtGeographicView.setText(geographicData);
    txtDOBView.setText(dob);
    txtTelephonesView.setText(phonenumber);
    txtTelephonesView1.setText(phonenumber1);
    txtFaxNumberView.setText(faxNumber);
    txtEmailAddressesView.setText(emailAddresses);
    txtEmailAddressesView1.setText(emailAddresses1);
    txtSSNView.setText(socialSecurityNumber);
    txtMRNView.setText(medicalRecordNumber);
    txtHealthPlanView.setText(healthPlanBeneficiaryNumber);
    txtBankAccNumView.setText(bankAccountNumbers);
    txtBankAccNumView1.setText(bankAccountNumbers1);
    txtLinkedInView.setText(linkedIn);
    txtIPaddressesView.setText(ipAddresses);
    txtIPaddressesView1.setText(ipAddresses1);
    
    ImageIcon imageicon = new ImageIcon(FullFacesPhotos);
    Image image = imageicon.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
    lblImageView.setIcon(new ImageIcon(image));
    
    ImageIcon imageicon1 = new ImageIcon(FullFacesPhotos1);
    Image image1 = imageicon1.getImage().getScaledInstance(120, 120, Image.SCALE_SMOOTH);
    lblImageView1.setIcon(new ImageIcon(image1));
    
    
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        jScrollPane1 = new javax.swing.JScrollPane();
        viewPersonProfileJPanel = new javax.swing.JPanel();
        lblName3 = new javax.swing.JLabel();
        txtNameView = new javax.swing.JTextField();
        lblGeagraphicData3 = new javax.swing.JLabel();
        txtGeographicView = new javax.swing.JTextField();
        lblDOB3 = new javax.swing.JLabel();
        txtDOBView = new javax.swing.JTextField();
        lblTelephones3 = new javax.swing.JLabel();
        txtTelephonesView = new javax.swing.JTextField();
        lblFaxNaumber3 = new javax.swing.JLabel();
        txtFaxNumberView = new javax.swing.JTextField();
        lblEmails6 = new javax.swing.JLabel();
        txtEmailAddressesView = new javax.swing.JTextField();
        lblSSN3 = new javax.swing.JLabel();
        txtSSNView = new javax.swing.JTextField();
        lblMRN3 = new javax.swing.JLabel();
        txtMRNView = new javax.swing.JTextField();
        lblHealthNumber3 = new javax.swing.JLabel();
        txtHealthPlanView = new javax.swing.JTextField();
        lblBankAccNum6 = new javax.swing.JLabel();
        txtBankAccNumView = new javax.swing.JTextField();
        lblLinkedIn3 = new javax.swing.JLabel();
        txtLinkedInView = new javax.swing.JTextField();
        lblIPAddresses6 = new javax.swing.JLabel();
        txtIPaddressesView = new javax.swing.JTextField();
        ViewPanel = new javax.swing.JLabel();
        lblImageView = new javax.swing.JLabel();
        txtTelephonesView1 = new javax.swing.JTextField();
        lblTelephoneNumberOptional4 = new javax.swing.JLabel();
        lblEmails9 = new javax.swing.JLabel();
        txtEmailAddressesView1 = new javax.swing.JTextField();
        lblBankAccNum9 = new javax.swing.JLabel();
        txtBankAccNumView1 = new javax.swing.JTextField();
        lblIPAddresses9 = new javax.swing.JLabel();
        txtIPaddressesView1 = new javax.swing.JTextField();
        lblImageView1 = new javax.swing.JLabel();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        lblName3.setText("Name");

        txtNameView.setEnabled(false);

        lblGeagraphicData3.setText("Geographic Data");

        txtGeographicView.setEnabled(false);

        lblDOB3.setText("Date of Birth");
        lblDOB3.setToolTipText("");

        txtDOBView.setEnabled(false);

        lblTelephones3.setText("Telephone Number");

        txtTelephonesView.setEnabled(false);

        lblFaxNaumber3.setText("Fax Number");

        txtFaxNumberView.setEnabled(false);

        lblEmails6.setText("Email Address");

        txtEmailAddressesView.setEnabled(false);

        lblSSN3.setText("Social Security Number");

        txtSSNView.setEnabled(false);

        lblMRN3.setText("Medical Record Number");

        txtMRNView.setEnabled(false);

        lblHealthNumber3.setText("Health Plan Beneficiary Number");

        txtHealthPlanView.setEnabled(false);

        lblBankAccNum6.setText("Bank Account Number");

        txtBankAccNumView.setEnabled(false);

        lblLinkedIn3.setText("LinkedIn");

        txtLinkedInView.setEnabled(false);

        lblIPAddresses6.setText("IP Address1");

        txtIPaddressesView.setEnabled(false);

        ViewPanel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        ViewPanel.setForeground(new java.awt.Color(51, 51, 255));
        ViewPanel.setText("View Person Profile");
        ViewPanel.setPreferredSize(new java.awt.Dimension(250, 40));

        txtTelephonesView1.setEnabled(false);

        lblTelephoneNumberOptional4.setText("Telephone Number(optional)");

        lblEmails9.setText("Email Address(Optional)");

        txtEmailAddressesView1.setEnabled(false);
        txtEmailAddressesView1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmailAddressesView1ActionPerformed(evt);
            }
        });

        lblBankAccNum9.setText("Bank Account Number(Optional)");

        txtBankAccNumView1.setEnabled(false);

        lblIPAddresses9.setText("IP Address2");

        txtIPaddressesView1.setEnabled(false);

        javax.swing.GroupLayout viewPersonProfileJPanelLayout = new javax.swing.GroupLayout(viewPersonProfileJPanel);
        viewPersonProfileJPanel.setLayout(viewPersonProfileJPanelLayout);
        viewPersonProfileJPanelLayout.setHorizontalGroup(
            viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(viewPersonProfileJPanelLayout.createSequentialGroup()
                .addGap(115, 115, 115)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(viewPersonProfileJPanelLayout.createSequentialGroup()
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lblMRN3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblSSN3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblIPAddresses6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblEmails6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblLinkedIn3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblBankAccNum6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblHealthNumber3, javax.swing.GroupLayout.DEFAULT_SIZE, 202, Short.MAX_VALUE)
                            .addComponent(lblFaxNaumber3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTelephones3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblDOB3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblGeagraphicData3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblName3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblTelephoneNumberOptional4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblEmails9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(66, 66, 66)
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtNameView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtGeographicView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDOBView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelephonesView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelephonesView1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtFaxNumberView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtHealthPlanView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBankAccNumView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLinkedInView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEmailAddressesView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIPaddressesView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEmailAddressesView1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtMRNView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtSSNView, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBankAccNumView1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtIPaddressesView1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(63, 63, 63)
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblImageView, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblImageView1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblBankAccNum9, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblIPAddresses9, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ViewPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        viewPersonProfileJPanelLayout.setVerticalGroup(
            viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(viewPersonProfileJPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ViewPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(viewPersonProfileJPanelLayout.createSequentialGroup()
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNameView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblName3))
                        .addGap(17, 17, 17)
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblGeagraphicData3)
                            .addComponent(txtGeographicView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtDOBView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDOB3)))
                    .addComponent(lblImageView, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(viewPersonProfileJPanelLayout.createSequentialGroup()
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTelephones3)
                            .addComponent(txtTelephonesView, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTelephonesView1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTelephoneNumberOptional4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblFaxNaumber3)
                            .addComponent(txtFaxNumberView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblImageView1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEmails6)
                    .addComponent(txtEmailAddressesView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtEmailAddressesView1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblEmails9))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSSNView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSSN3))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblMRN3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMRNView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtHealthPlanView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblHealthNumber3))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBankAccNumView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBankAccNum6))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBankAccNumView1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblBankAccNum9))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblLinkedIn3)
                    .addComponent(txtLinkedInView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblIPAddresses6)
                    .addComponent(txtIPaddressesView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(viewPersonProfileJPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblIPAddresses9)
                    .addComponent(txtIPaddressesView1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane1.setViewportView(viewPersonProfileJPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1510, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 879, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtEmailAddressesView1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmailAddressesView1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmailAddressesView1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ViewPanel;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblBankAccNum6;
    private javax.swing.JLabel lblBankAccNum9;
    private javax.swing.JLabel lblDOB3;
    private javax.swing.JLabel lblEmails6;
    private javax.swing.JLabel lblEmails9;
    private javax.swing.JLabel lblFaxNaumber3;
    private javax.swing.JLabel lblGeagraphicData3;
    private javax.swing.JLabel lblHealthNumber3;
    private javax.swing.JLabel lblIPAddresses6;
    private javax.swing.JLabel lblIPAddresses9;
    private javax.swing.JLabel lblImageView;
    private javax.swing.JLabel lblImageView1;
    private javax.swing.JLabel lblLinkedIn3;
    private javax.swing.JLabel lblMRN3;
    private javax.swing.JLabel lblName3;
    private javax.swing.JLabel lblSSN3;
    private javax.swing.JLabel lblTelephoneNumberOptional4;
    private javax.swing.JLabel lblTelephones3;
    private javax.swing.JTextField txtBankAccNumView;
    private javax.swing.JTextField txtBankAccNumView1;
    private javax.swing.JTextField txtDOBView;
    private javax.swing.JTextField txtEmailAddressesView;
    private javax.swing.JTextField txtEmailAddressesView1;
    private javax.swing.JTextField txtFaxNumberView;
    private javax.swing.JTextField txtGeographicView;
    private javax.swing.JTextField txtHealthPlanView;
    private javax.swing.JTextField txtIPaddressesView;
    private javax.swing.JTextField txtIPaddressesView1;
    private javax.swing.JTextField txtLinkedInView;
    private javax.swing.JTextField txtMRNView;
    private javax.swing.JTextField txtNameView;
    private javax.swing.JTextField txtSSNView;
    private javax.swing.JTextField txtTelephonesView;
    private javax.swing.JTextField txtTelephonesView1;
    private javax.swing.JPanel viewPersonProfileJPanel;
    // End of variables declaration//GEN-END:variables
}
