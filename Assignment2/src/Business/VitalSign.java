/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author navee
 */
public class VitalSign {
    
    private int respiratoryRate;
    private int heartRate; 
    private double temperature;
    private double bloodpressure;
    private int pulse;
    private double weightInlbs;
    private double weightInkilos;
    private Date date;
    private String normal;

    public VitalSign() {
        this.date = new Date();
        this.normal = isNormal();
    }
    
    

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(double bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }


    public int getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(int respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public int getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(int heartRate) {
        this.heartRate = heartRate;
    }

    public double getWeightInlbs() {
        return weightInlbs;
    }

    public void setWeightInlbs(double weightInlbs) {
        this.weightInlbs = weightInlbs;
    }

    public double getWeightInkilos() {
        return weightInkilos;
    }

    public void setWeightInkilos(double weightInkilos) {
        this.weightInkilos = weightInkilos;
    }

    public String getNormal() {
        return normal;
    }


    
    public String isNormal(){
        if((this.heartRate<70 || this.heartRate>80)||(this.bloodpressure>100||this.bloodpressure<80)||(this.weightInlbs>140||this.weightInlbs<35)
                ||(this.respiratoryRate>60||this.respiratoryRate<40)){
            return "Abnormal";
        }
        else{
        return "Normal";
        }
    }
    

@Override
public String toString(){
    return String.valueOf(this.date);
}

    
    
    
}
