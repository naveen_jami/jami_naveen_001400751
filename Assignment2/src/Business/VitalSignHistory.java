/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.awt.List;
import java.util.ArrayList;


/**
 *
 * @author navee
 */
public class VitalSignHistory {
    
    private ArrayList<VitalSign> vsh;

    public VitalSignHistory() {
        this.vsh =  new ArrayList<>();
    }

    public ArrayList<VitalSign> getVsh() {
        return vsh;
    }

    public void setVsh(ArrayList<VitalSign> vsh) {
        this.vsh = vsh;
    }
    
    public VitalSign addVitalSign(){
        
        VitalSign vs = new VitalSign();
        vsh.add(vs);
        return vs;
        
    }
    
    public void deleteVitalSign(VitalSign vs){
        
        vsh.remove(vs);
    }


    public ArrayList<VitalSign> getAbnormalVshList(double maxbp, double minbp) {
     //To change body of generated methods, choose Tools | Templates.
     
     ArrayList<VitalSign> abnlist = new ArrayList<>();
     
     for(VitalSign vs: vsh){
        if(vs.getBloodpressure()<minbp || vs.getBloodpressure()>maxbp) {
            abnlist.add(vs);
        }
     }
     
     
     
     return abnlist;
    }
     
    
    
}
