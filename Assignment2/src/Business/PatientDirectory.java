/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author navee
 */
public class PatientDirectory {
    
    private ArrayList<Patient> patients;

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    public void setPatients(ArrayList<Patient> patients) {
        this.patients = patients;
    }

    public PatientDirectory() {
        this.patients = new ArrayList<>();
    }
    
    public void addPatient(Patient patient){
       this.patients.add(patient);
    }
    
    
    
    
    
}
