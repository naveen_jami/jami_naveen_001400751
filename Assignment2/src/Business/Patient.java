/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author navee
 */
public class Patient {
    
    private String patientName;
    private int patientId;
    private String doctorName;
    private String prefPharmacy;
    private VitalSignHistory vsh;

    public Patient() {
        vsh = new VitalSignHistory();
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }


    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getPrefPharmacy() {
        return prefPharmacy;
    }

    public void setPrefPharmacy(String prefPharmacy) {
        this.prefPharmacy = prefPharmacy;
    }

    public VitalSignHistory getVsh() {
        return vsh;
    }

    public void setVsh(VitalSignHistory vsh) {
        this.vsh = vsh;
    }
    
 @Override
 public String toString(){
     return String.valueOf(patientId);
 }
    
}
