/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author navee
 */
public class VitalSign {
    
    private double temperature;
    private double bloodpressure;
    private int pulse;
    private Date date;

    public VitalSign() {
        this.date = new Date();
    }
    
    

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getBloodpressure() {
        return bloodpressure;
    }

    public void setBloodpressure(double bloodpressure) {
        this.bloodpressure = bloodpressure;
    }

    public int getPulse() {
        return pulse;
    }

    public void setPulse(int pulse) {
        this.pulse = pulse;
    }

@Override
public String toString(){
    return String.valueOf(this.date);
}
    
    
    
}
